let images = [
    { link: "https://stackoverflow.com/users/login?ssrc=channels&returnurl=%2fc%2finventhys%2fquestions%3ftab%3dactive%26pagesize%3d50", img: "../assets/images/stack.png" },
    { link: "https://drive.google.com/drive/folders/1ITqaCCRP3LxW14YEM8cU1UtYjdHbb4ty", img: "../assets/images/drive.png" },
    { link: "https://gestion-achats.bubbleapps.io/login", img: "../assets/images/bubble.png" },
    { link: "https://docs.google.com/spreadsheets/d/1B3Oikz2ocJd2fMwSMjrLnmLhjdGefIcnGhHvM-U4gKA/edit#gid=0", img: "../assets/images/planning.png" },
    { link: "https://plateforme.eurecia.com/eurecia/index.html#/old?url=planningVacation%2Fplanning.do%3FidSer[…]55Ddbf9905313d2964b0113d2e5a3751868%26mdleid%3D1", img: "../assets/images/eurecia.png" },
    { link: "https://docs.google.com/spreadsheets/d/1_A8NtZ7uqRWqKidCgE1XILWslssu5ItZnqY2AP0jrzU/edit#gid=2015171827", img: "../assets/images/caddie.png" },
    { link: "https://docs.google.com/spreadsheets/d/1wS88Me61xGUMzOZiNplihmxeKA1Ox-uWR55GV0hqaLk/edit#gid=0", img: "../assets/images/food.png" },
  ];
  
  let container = document.createElement("div");
  
  container.className="containerLogos"
  for (const image of images) {
    let linkElement = document.createElement("a");
    linkElement.href = image.link;
    linkElement.target = "_blank"
    
    let imageElement = document.createElement("img");
    imageElement.src = image.img;
    
    linkElement.appendChild(imageElement);
    
    container.appendChild(linkElement);
    
    imageElement.className = "logos"
  }
  
  
  document.body.appendChild(container);