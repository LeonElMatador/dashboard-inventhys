// import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
// // import { getAuth, signInWithPopup, signInWithRedirect, GoogleAuthProvider} from "firebase/auth";
// import {
//   getAuth,
//   signInWithPopup,
//   signInWithRedirect,
//   GoogleAuthProvider,
// } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

// import { auth } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js'

const firebaseConfig = {
    apiKey: "AIzaSyAGu02_3C_Y_D5Wgsbnw2_g_6q8-2H7J5c",
    authDomain: "auth-app-c326b.firebaseapp.com",
    projectId: "auth-app-c326b",
    storageBucket: "auth-app-c326b.appspot.com",
    messagingSenderId: "749014424599",
    appId: "1:749014424599:web:ce5a1f5bfb103b6fac5052"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var provider = new firebase.auth.GoogleAuthProvider();

function popUp() {
  console.log("popUp");
  firebase.auth().signInWithPopup(provider)
    .then((result) => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const credential = GoogleAuthProvider.credentialFromResult(result);
      const token = credential.accessToken;
      // The signed-in user info.
      const user = result.user;
      // ...
    })
    .catch((error) => {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
      const email = error.email;
      // The AuthCredential type that was used.
      const credential = GoogleAuthProvider.credentialFromError(error);
      // ...
    });
}


const btn = document.getElementById("google")

btn.addEventListener('click', () => popUp())